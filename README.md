# picoblock

Fast & lightweight system wide (web and app) ad blocker for iOS and Windows 10.


## What's picoblock?

picoblock is a Proxy Auto Configuration (PAC) File that can block ads and trackers.

It was designed to be fast and lightweight focusing on iOS and Windows 10 mobile devices.

picoblock filters ad traffic directly at the network level and sends it to Google's blackhole sever (8.8.8.8:53), while keeping the rest direct.


## Proxy Auto Configuration (PAC) File

### To Use: GitLab Host

Set your network Proxy Auto Configuration setting to:

> `https://gitlab.com/victorbayas/picoblock/raw/master/pac/picoblock.min.js`

Read our detailed [setup guide](https://gitlab.com/victorbayas/picoblock/wikis/Setup-Guide)

***Advantages***

* Works on any mobile or desktop device on any WiFi network worldwide.
* GitLab server; private web server not necessary.

***Disadvantages***

* Does not work on mobile data networks.
* Reliance on a third-party (me) for pass/block rule sets, updates, and `picoblock.min.js` integrity.

### To Use: Localhost

Download the [picoblock.min.js](https://gitlab.com/victorbayas/picoblock/raw/master/pac/picoblock.min.js) file.

On macOS (without Server.app):

```
sudo cp ~/Downloads/picoblock.min.js /Library/WebServer/Documents
sudo apachectl start
```

Set your network Proxy Auto Configuration setting to:

> `http://localhost/picoblock.min.js` or `http://host-ip-address/picoblock.min.js`

***Advantages***

* Works for any mobile or desktop device on your LAN.
* Works with an upstream proxy if specified in the `picoblock.min.js` file.
* Individual update control and customization of the `picoblock.min.js` file and filter rules.
* Possible internet access if port 80 exposed outside the LAN firewall.

***Disadvantages***

* Does not work on mobile data networks.
* No internet access unless port forwarding to host is used.

### To Use: VPN

Configure an OpenVPN Server to use the `picoblock.min.js` file hosted on your LAN.

This is the best option.

***Advantages***

* Works on any mobile or desktop device on any mobile data or WiFi network worldwide.
* Individual update control and customization of the `proxy.pac` file and filter rules.
* Security and privacy benefits of VPNs.

***Disadvantages***

* Necessity of VPN server.



## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.


## Public Service Announcement 

picoblock is configured to block advertisements and some trackers at the network level. Many websites now offer an additional way to block ads: subscribe to their content. Security and privacy will always necessitate ad blocking, but now that this software has become mainstream with mainstream effects, ad blocker users must consider the [potential impact](http://arstechnica.com/business/2010/03/why-ad-blocking-is-devastating-to-the-sites-you-love/) of ad blocking on the writers and publications that are important to them.