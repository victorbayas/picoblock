// picoblock v1.0 - database 07/09/17
// Copyright 2017 Victor Bayas

function FindProxyForURL(url, host) {
    if (
        // Common ad and tracking servers
        shExpMatch(host, "admob.com") ||
        shExpMatch(host, "*.admob.com") ||
        shExpMatch(host, "adsnative.com") ||
        shExpMatch(host, "*.adsnative.com") ||
        shExpMatch(host, "affiliate-program.amazon.com") ||
        shExpMatch(host, "advertiseurl.com") ||
        shExpMatch(host, "*.advertiseurl.com") ||
        shExpMatch(host, "airpush.com") ||
        shExpMatch(host, "*.airpush.com") ||
        shExpMatch(host, "appnext.com") ||
        shExpMatch(host, "*.appnext.com") ||
        shExpMatch(host, "brucelead.com") ||
        shExpMatch(host, "*.brucelead.com") ||
        shExpMatch(host, "carbonads.net") ||
        shExpMatch(host, "*.carbonads.net") ||
        shExpMatch(host, "chartboost.com") ||
        shExpMatch(host, "*.chartboost.com") ||
        shExpMatch(host, "chitika.com") ||
        shExpMatch(host, "*.chitika.com") ||
        shExpMatch(host, "cpm10.com") ||
        shExpMatch(host, "*.cpm10.com") ||
        shExpMatch(host, "doubleclick.net") ||
        shExpMatch(host, "*.doubleclick.net") ||
        shExpMatch(host, "exoclick.com") ||
        shExpMatch(host, "*.exoclick.com") ||
        shExpMatch(host, "flurry.com") ||
        shExpMatch(host, "*.flurry.com") ||
        shExpMatch(host, "google-analytics.com") ||
        shExpMatch(host, "*.google-analytics.com") ||
        shExpMatch(host, "googleadservices.com") ||
        shExpMatch(host, "*.googleadservices.com") ||
        shExpMatch(host, "googlesyndication.com") ||
        shExpMatch(host, "*.googlesyndication.com") ||
        shExpMatch(host, "hilltopads.com") ||
        shExpMatch(host, "*.hilltopads.com") ||
        shExpMatch(host, "juicyads.com") ||
        shExpMatch(host, "*.juicyads.com") ||
        shExpMatch(host, "leadzu.com") ||
        shExpMatch(host, "*.leadzu.com") ||
        shExpMatch(host, "mbtrx.com") ||
        shExpMatch(host, "mdotm.com") ||
        shExpMatch(host, "mobpartner.com") ||
        shExpMatch(host, "*.mobpartner.com") ||
        shExpMatch(host, "mocean.mobi") ||
        shExpMatch(host, "*.mocean.mobi") ||
        shExpMatch(host, "mojiva.com") ||
        shExpMatch(host, "*.mojiva.com") ||
        shExpMatch(host, "outbrain.com") ||
        shExpMatch(host, "*.outbrain.com") ||
        shExpMatch(host, "popcash.net") ||
        shExpMatch(host, "*.popcash.net") ||
        shExpMatch(host, "propellerads.com") ||
        shExpMatch(host, "*.propellerads.com") ||
        shExpMatch(host, "reporo.com") ||
        shExpMatch(host, "*.reporo.com") ||
        shExpMatch(host, "revcontent.com") ||
        shExpMatch(host, "*.revcontent.com") ||
        shExpMatch(host, "smartadserver.com") ||
        shExpMatch(host, "*.smartadserver.com") ||
        shExpMatch(host, "spiroox.com") ||
        shExpMatch(host, "*.spiroox.com") ||
        shExpMatch(host, "taboola.com") ||
        shExpMatch(host, "*.taboola.com") ||
        shExpMatch(host, "tradeadexchange.com") ||
        shExpMatch(host, "*.tradeadexchange.com") ||
        shExpMatch(host, "trafficfactory.biz") ||
        shExpMatch(host, "*.trafficfactory.biz") ||
        shExpMatch(host, "trafficjunky.net") ||
        shExpMatch(host, "*.trafficjunky.net") ||
        shExpMatch(host, "unityads.unity3d.com") ||
        shExpMatch(host, "viralcpm.com") ||
        shExpMatch(host, "*.viralcpm.com") ||
        shExpMatch(host, "vungle.com") ||
        shExpMatch(host, "*.vungle.com") ||

        // Universal rules
        shExpMatch(host, "*/ads/*") ||
        shExpMatch(host, "*/banner/*") ||
        shExpMatch(host, "*/viewad/*") ||
        shExpMatch(host, "ad.*") ||
        shExpMatch(host, "ads.*") ||
        shExpMatch(host, "ads1.*") ||
        shExpMatch(host, "ads2.*") ||
        shExpMatch(host, "ads3.*") ||
        shExpMatch(host, "ads4.*") ||
        shExpMatch(host, "ads5.*") ||
        shExpMatch(host, "adserve.*") ||
        shExpMatch(host, "adserver.*") ||
        shExpMatch(host, "adsrv.*") ||
        shExpMatch(host, "adv.*") ||
        shExpMatch(host, "analytics.*") ||
        shExpMatch(host, "banner.*") ||
        shExpMatch(host, "banners.*") ||
        shExpMatch(host, "campaign.*") ||
        shExpMatch(host, "cash.*") ||
        shExpMatch(host, "click.*") ||
        shExpMatch(host, "clicks.*") ||
        shExpMatch(host, "dtrk.*") ||
        shExpMatch(host, "mobileads.*") ||
        shExpMatch(host, "pop.*") ||
        shExpMatch(host, "popunder.*") ||
        shExpMatch(host, "popup.*") ||
        shExpMatch(host, "track.*") ||
        shExpMatch(host, "tracking.*") ||
        shExpMatch(host, "trk.*")
    ) {
        // Sends matched traffic to Google's blackhole server
        return "PROXY 8.8.8.8:53";
    }
    else {
        // Non matched traffic goes direct
        return "DIRECT";
    }
}